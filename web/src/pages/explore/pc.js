import React from "react";

import GamesDisplay from "../../components/games/GamesDisplay";

const PcGames = () => (
    <GamesDisplay
        platformId="6"
        platform="pc"
        platformTitle="PC" />
)

export default PcGames;