import React from "react";

import GamesDisplay from "../../components/games/GamesDisplay";

const Ps4Games = () => (
    <GamesDisplay
        platformId="48"
        platform="ps4"
        platformTitle="PS4" />
)

export default Ps4Games;