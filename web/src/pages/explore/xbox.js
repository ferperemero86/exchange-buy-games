import React from "react";

import GamesDisplay from "../../components/games/GamesDisplay";

const PcGames = () => (
    <GamesDisplay
        platformId="49"
        platform="xbox"
        platformTitle="XBOX" />
)

export default PcGames;